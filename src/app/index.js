const $ = require("jquery");
require("jquery-ui");
require("jquery-ui-dist/jquery-ui.min.css");
require("jquery-ui-dist/jquery-ui.theme.min.css");

import './styles/bdwd.scss';
import { BDWD } from './bdwd';

const bdwd = new BDWD();
window.bdwd = bdwd;