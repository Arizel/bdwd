import BpmnModeler from 'bpmn-js/lib/Modeler';
import PropertiesPanel from 'bpmn-js-properties-panel';
import PropertiesProviderModule from 'bpmn-js-properties-panel/lib/provider/camunda';
import camundaModdleDescriptor from 'camunda-bpmn-moddle/resources/camunda';
import { BPMNClient } from './bpmn-client';
import emptyDiagram from './assets/new.bpmn';

/**
 * Base class for the Big Data Workflow Designer. Implements all logic and wraps the bpmn-js framework.
 */
export class BDWD {
	bpmnClient = null;
	viewer = null;
	dirty = false;
	currentFile = null;

	constructor() {
		this.bpmnClient = new BPMNClient();

		// Initialize bpmn-js
		this.viewer = new BpmnModeler({
			container: '#js-canvas',
			propertiesPanel: {
				parent: '#js-properties-panel'
			},
			additionalModules: [
				PropertiesPanel,
				PropertiesProviderModule
			],
			moddleExtensions: {
				camunda: camundaModdleDescriptor
			}
		});

		// Mark state as dirty when diagram is changed
		this.viewer.on('commandStack.changed', () => { this.dirty = true; });

		// Wire up nav-bar icons
		$('.icon.new').on('click', $.proxy(function(){
			this.actionNew();
		},this));
		$('.icon.open').on('click', $.proxy(function(){
			this.actionOpen();
		},this));
		$('.icon.save').on('click', $.proxy(function(){
			this.actionSave();
		},this));
		$('.icon.saveas').on('click', $.proxy(function(){
			this.actionSaveAs();
		},this));
		$('.icon.info').on('click', $.proxy(function(){
			this.actionInfo();
		},this));

		// Load an empty diagram
		this.actionNew();
	}

	/**
	 * An action to create a new diagram.
	 */
	actionNew() {

		// As for confirmation if the current diagram state has unsaved changes
		if (this.dirty) {

			// Get dialog instance or create a new one
			const dialogElement = $('#dialog-confirm-discard');
			const _self = this;
			dialogElement.dialog({
				autoOpen: false,
				resizable: false,
				height: "auto",
				width: 400,
				modal: true,
				buttons: {
					"OK": function() {
						_self._loadDiagram(emptyDiagram);
						_self.currentFile = null;
						$(this).dialog( "close" );
					},
					Cancel: function() {
						$(this).dialog( "close" );
					}
				}
			});

			// Open the dialog
			dialogElement.dialog('instance').open();

		// No changes; just initialize modeller with the "new" template
		} else {
			this.selectedFile = null;
			this._loadDiagram(emptyDiagram);
		}
	}

	/**
	 * Action to open an existing diagram.
	 */
	actionOpen() {
		const _self = this;
		const handleDiagramOpening = function (dialogReference) {
			const selectedFile = $('.files-select').val();

			if (selectedFile) {
				_self.bpmnClient.read(selectedFile).then(function(data) {
					_self._loadDiagram(unescape(data));
					_self.currentFile = selectedFile;
					dialogReference.dialog( "close" );
				}).catch(function (err) {
					_self.showError(err);
				});
			} else {
				alert('Please select a file.');
			}
		};

		// Get dialog instance or create a new one
		$( "#dialog-open" ).dialog({
			autoOpen: false,
			resizable: false,
			height: "auto",
			width: 400,
			modal: true,
			buttons: {
				"Open": function() {
					handleDiagramOpening($(this));
				},
				Cancel: function() {
					$(this).dialog( "close" );
				}
			}
		});

		$('.files-select').empty();
		this.bpmnClient.readDirectory().then(function(data) {
			data.forEach(function (entry) {
				$('.files-select').append(`<option value="${entry}">${entry}</option>`);
			});
		}).catch(function (err) {
			_self.showError(err);
		});
		
		// Open the dialog
		$('#dialog-open').dialog('instance').open();
	}

	/**
	 * Action to save current diagram under the same file.
	 */
	actionSave() {
		const _self = this;

		if (this.dirty) {
			
			// We are working already on a file; just update it
			if (this.currentFile) {
				this.viewer.saveXML(null, function(err, xml) {
					if (!err) {
						_self.bpmnClient.update(_self.currentFile, xml);
						_self.dirty = false;
					} else {
						_self.showError(err);
					}
				});

				// New file; show file name dialog before saving it
				// Calling saveAs here because of same functionality
			} else {
				this.actionSaveAs();
			}
		}
	}
	
	/**
	 * Action to save a file under a new file name
	 */
	actionSaveAs() {
		const prepareFileName = (fileName) => fileName.endsWith('.bpmn') ? fileName : `${fileName}.bpmn`;
		const fileNameInput = $('#dialog-save .file-name');
		const dialogElement = $('#dialog-save');
		const _self = this;

		// Get dialog instance or create a new one
		fileNameInput.val('');
		dialogElement.dialog({
			autoOpen: false,
			resizable: false,
			height: "auto",
			width: 400,
			modal: true,
			buttons: {
				"Save": function() {
					const fileName = prepareFileName(fileNameInput.val());
					_self._handleSaveAsOverwritecheck(fileName);
					$(this).dialog( "close" );
				},
				Cancel: function() {
					$(this).dialog( "close" );
				}
			}
		});

		// Open the dialog
		dialogElement.dialog('instance').open();
	}

	/**
	 * Shows the info dialog
	 */
	actionInfo() {
		const dialogElement = $('#dialog-info');
		dialogElement.dialog();
	}

	/**
	 * Shows an error message.
	 * @param {string} errorMessage Error message to show
	 */
	showError(errorMessage) {
		$( "#dialog-error .error-message" ).text(errorMessage);

		// Get dialog instance or create a new one
		$( "#dialog-error" ).dialog({
			autoOpen: false,
			resizable: false,
			height: "auto",
			width: 400,
			modal: true,
			buttons: {
				"OK": function() {
					$(this).dialog( "close" );
				}
			}
		});
		
		// Open the dialog
		$('#dialog-open').dialog('instance').open();
	}

	/**
	 * Promts the user if he realy wants to overwrite an already existing file
	 * @param {string} fileName 
	 */
	_handleSaveAsOverwritecheck(fileName) {
		const _self = this;
		this.bpmnClient.read(fileName)
			.then(function(data) {
				if (data.length > 0) {
					const dialogElement = $('#dialog-confirm-overwrite');

					// Get dialog instance or create a new one
					dialogElement.dialog({
						autoOpen: false,
						resizable: false,
						height: "auto",
						width: 400,
						modal: true,
						buttons: {
							"OK": function() {
								_self.viewer.saveXML(null, function(err, xml) {
									if (!err) {
										_self.bpmnClient.create(fileName, xml);
										_self.dirty = false;
									} else {
										_self.showError(err);
									}
								});
								$(this).dialog( "close" );
							},
							Cancel: function() {
								$(this).dialog( "close" );
							}
						}
					});
			
					// Open the dialog
					dialogElement.dialog('instance').open();
				
				// File doesn't exist yet. Create it
				} else {
					_self.viewer.saveXML(null, function(err, xml) {
						if (!err) {
							_self.bpmnClient.create(fileName, xml);
							_self.dirty = false;
						} else {
							_self.showError(err);
						}
					});
				}
			})
			.catch(function (err) {
				_self.showError(err);
			});
	}

	/**
	 * Loads a diagram into the viewer.
	 * @private
	 * @param {string} diagram Diagram to load
	 */
	_loadDiagram(diagram) {
		const _self = this;
		this.viewer.importXML(diagram, function (err) {
			if (!err) {
				console.log('success!');
				_self.viewer.get('canvas').zoom('fit-viewport');
				_self.dirty = false;
			} else {
				_self.showError(err);
			}
		});
	}
}