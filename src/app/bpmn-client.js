import * as config from './bpmn-client.config';

/**
 * REST client for communication with the Big Data BPMN Mediator.
 */
export class BPMNClient {

	/**
	 * Creates a file on the server.
	 * @param {string} fileName Name of the file to create
	 * @param {string} fileContent Content of the file to create
	 * @returns {Object} Content of the created file
	 */
	create(fileName, fileContent) {
		return fetch(`${config.host}${config.path}/${fileName}`, {
			method: "POST",
			mode: "cors",
			headers: {
				"Content-Type": "text/plain",
			},
			body: fileContent
		})
			.then(function(response) {
				return response.text();
			})
			.then(function(textResponse) {
				return textResponse;
			});
	}

	/**
	 * Reads the files directory to get a list of all available diagrams.
	 * @returns {Array} An array of file names as Promise
	 */
	readDirectory() {
		return fetch(`${config.host}${config.path}`, {
			method: "GET",
			mode: "cors"
		})
			.then(function(response) {
				return response.json();
			})
			.then(function(jsonResponse) {
				return jsonResponse;
			});
	}

	/**
	 * Reads a file and returns its content.
	 * @param {string} fileName Name of the file to read
	 * @returns {string} File content as Promise
	 */
	read(fileName) {
		return fetch(`${config.host}${config.path}/${fileName}`, {
			method: "GET",
			mode: "cors"
		})
			.then(function(response) {
				return response.text();
			})
			.then(function(textResponse) {
				return escape(textResponse);
			});
	}

	/**
	 * Updates a file on the server.
	 * @param {string} fileName Name of the file to update
	 * @param {*} fileContent Content of the file to update
	 * @returns {Object} Content of the updated file
	 */
	update(fileName, fileContent) {
		return fetch(`${config.host}${config.path}/${fileName}`, {
			method: "PUT",
			mode: "cors",
			headers: {
				"Content-Type": "text/plain",
			},
			body: fileContent
		})
			.then(function(response) {
				return response.text();
			})
			.then(function(textResponse) {
				return textResponse;
			});
	}

	/**
	 * Deletes a file from remote.
	 * @param {string} fileName Name of the file to delete
	 * @returns {boolean} TRUE for a successful delete; FALSE otherwise
	 */
	delete(fileName) {
		return fetch(`${config.host}${config.path}/${fileName}`, {
			method: "DELETE",
			mode: "cors"
		})
			.then(function(response) {
				return response.json();
			})
			.then(function(jsonResponse) {
				return jsonResponse;
			});
	}
}