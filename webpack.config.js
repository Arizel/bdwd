const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HTMLInlineCSSWebpackPlugin = require("html-inline-css-webpack-plugin").default;
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

module.exports = {
	entry: __dirname + "/src/app/index.js",
	resolve: {
		alias: {
			'jquery-ui': 'jquery-ui-dist/jquery-ui.js'
		}
	},
	output: {
		path: __dirname + '/dist',
		filename: 'bundle.js',
		publicPath: '/'
	},
	optimization: {
		minimizer: [
			new UglifyJsPlugin({
				parallel: true
			}),
			new OptimizeCSSAssetsPlugin({
				cssProcessor: require('cssnano'),
				cssProcessorPluginOptions: {
					preset: ['default', { discardComments: { removeAll: true } }],
				},
				canPrint: true
			})
		],
	},
	module: {
		rules: [
			{
				test: /\.(xml|bpmn)$/i,
				use: 'raw-loader',
			},
			{
				test: /\.js$/,
				use: 'babel-loader',
				exclude: [
					/node_modules/
				]
			},
			{
				test: /\.(png|jpg|gif)$/i,
				use: [
					{
						loader: 'url-loader',
						options: {
							limit: 8192
						}
					}
				]
			},
			{
				test: /\.(sass|scss|css)$/,
				use: [{
					loader: MiniCssExtractPlugin.loader
				}, {
					loader: "css-loader",
					options: {
						url: true
					}
				}, {
					loader: "sass-loader"
				}]
			}
		]
	},

	plugins: [
		new HtmlWebpackPlugin({
			template: __dirname + "/src/app/index.html",
			inject: 'body',
			inlineSource: '.(js)$'
		}),
		new HtmlWebpackInlineSourcePlugin(),
		new MiniCssExtractPlugin({
			filename: "[name].css",
			chunkFilename: "[id].css"
		}),
		new HTMLInlineCSSWebpackPlugin(),
		new webpack.ProvidePlugin({
			"$": "jquery",
			"jQuery": "jquery",
			"window.jQuery": "jquery"
		})
	],

	devServer: {
		contentBase: './src/app',
		port: 7700,
	}
};
