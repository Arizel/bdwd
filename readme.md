# Big Data Workflow Modeller
A modeller to create BPMN diagrams and store/load them from a [BDBM server](https://bitbucket.org/Arizel/bdbm/src).

## How to build and run
This is a webpack based javascript application. You need a system with NodeJS v8.11.4+ to build and run this.  
**Build:** Before the first build, type `npm install` into a console in the root directory. Afterwards enter `webpack`  
**Run:** Type `npm start` into a console in the root directory. This starts a server at http://localhost:7700/

## Configuration
The URL and path of the BDBM endpoint are configured in src/app/bpmn-client.config.json. Rebuild after changing them.

## Usage
Call the URL in any modern browser. 
